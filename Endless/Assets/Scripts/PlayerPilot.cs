﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPilot : MonoBehaviour
{

    public float speed = 98.0f;

    // Start is called before the first frame update
    void Start ()
    {

    }

    // Update is called once per frame
    void Update ()
    {

        Vector3 moveCamTo = transform.position - transform.forward * 10.0f + Vector3.up * 5.0f;
        float bias = 0.96f;
        Camera.main.transform.position = Camera.main.transform.position * bias + moveCamTo * (1.0f - bias);
        Camera.main.transform.LookAt (transform.position + transform.forward * 30.0f);

        transform.position += transform.forward * Time.deltaTime * speed;

        speed -= transform.forward.y * Time.deltaTime * 50.0f;

        if (speed < 35.0f)
        {
            speed = 35.0f;
        }

        //if (speed > 60.0f)
        //{
        //   speed = 60.0f;
        //}

        transform.Rotate (-Input.GetAxis ("Vertical"), 0.0f, Input.GetAxis ("Horizontal"));

        //float terrainHeightWhereWeAre = 
    }
}